#!/usr/bin/env bash

apt-get update
apt-get upgrade -y
apt-get install -y \
    build-essential \
    automake automake \
    libpulse-dev \
    libx11-dev \
    libxinerama-dev \
    libxrandr-dev \
    libxxf86vm-dev \
    libxcursor-dev \
    libxi-dev \
    libasound2-dev \
    libxss-dev \
    libgl1-mesa-dev \
    libudev-dev \
    chrpath \
    vim \
    cmake-curses-gui \
    git
